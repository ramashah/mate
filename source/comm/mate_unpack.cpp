#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Unpack(const void* source, int size, int* position, void* dest, int count, MPI_Datatype mpiType, MPI_Comm comm)
{
	int ret = MPI_Unpack(source, size, position, dest, count, mpiType, comm);
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Unpack Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
