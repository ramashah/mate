#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


void Mate_OpExec(void *a, const void *b, int count, MPI_Datatype datatype, MPI_Op op)
{
  if (op != MPI_MIN && op != MPI_MAX && op != MPI_SUM)
  {
    fprintf(stderr, "[MATE] Error: Only MPI_MIN, MPI_MAX, MPI_SUM operations\n are currently supported by Mate for reductions.\n");
    exit(-1);
  }
	if (datatype != MPI_DOUBLE && datatype != MPI_INT && datatype != MPI_LONG)	{ fprintf(stderr, "[MATE] Error: Only MPI_DOUBLE, MPI_INT, MPI_LONG operations\n   are currently supported by MATE for reductions.\n"); MPI_Abort(-1, MPI_COMM_WORLD);}

  if (op == MPI_MIN)
  {
    if (datatype == MPI_DOUBLE)
    {
      for(int i=0; i<count; i++) ((double*) a)[i] = ((double*)a)[i] < ((double*)b)[i] ? ((double*)a)[i]:((double*)b)[i];
    }
    if (datatype == MPI_INT)
    {
      for(int i=0; i<count; i++) ((int*) a)[i] = ((int*)a)[i] < ((int*)b)[i] ? ((int*)a)[i]:((int*)b)[i];
    }
    if (datatype == MPI_LONG)
    {
      for(int i=0; i<count; i++) ((long*) a)[i] = ((long*)a)[i] < ((long*)b)[i] ? ((long*)a)[i]:((long*)b)[i];
    };
  }

  if (op == MPI_MAX)
  {
    if (datatype == MPI_DOUBLE)
    {
      for(int i=0; i<count; i++) ((double*) a)[i] = ((double*)a)[i] > ((double*)b)[i] ? ((double*)a)[i]:((double*)b)[i];
    }
    if (datatype == MPI_INT)
    {
      for(int i=0; i<count; i++) ((int*) a)[i] = ((int*)a)[i] > ((int*)b)[i] ? ((int*)a)[i]:((int*)b)[i];
    }
    if (datatype == MPI_LONG)
    {
      for(int i=0; i<count; i++) ((long*) a)[i] = ((long*)a)[i] > ((long*)b)[i] ? ((long*)a)[i]:((long*)b)[i];
    }
  }

  if (op == MPI_SUM)
  {
    if (datatype == MPI_DOUBLE)
    {
      for(int i=0; i<count; i++) ((double*) a)[i] = ((double*)a)[i] + ((double*)b)[i];
    }
    if (datatype == MPI_INT)
    {
      for(int i=0; i<count; i++) ((int*) a)[i] = ((int*)a)[i] + ((int*)b)[i];
    }
    if (datatype == MPI_LONG)
    {
      for(int i=0; i<count; i++) ((long*) a)[i] = ((long*)a)[i] + ((long*)b)[i];
    }
  }
}
