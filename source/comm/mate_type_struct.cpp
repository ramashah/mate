#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_struct(int count, int *array_of_blocklengths, MPI_Aint *array_of_displacements, MPI_Datatype *array_of_types, MPI_Datatype *newtype)
{
	_MPILock.lock();
  int ret =  MPI_Type_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, newtype);
  _MPILock.unlock();
  if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Type_struct Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
