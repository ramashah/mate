#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


// Mate_Barrier replaces MPI_Barrier. It calls LocalBarrier, but enforces a calls a global MPI_Barrier when the last task reaches rendezvous.
int Mate_Barrier(MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
	if(currentTask->_localId == 0) _isGlobalBarrier = true;
	int ret = Mate_LocalBarrier();
  return ret;
}

extern "C" int mate_barrier_(MPI_Comm* comm) { return Mate_Barrier(*comm); }
