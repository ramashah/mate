#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _gather_buffer;

int Mate_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int localTaskCount = _runtime->_local_rank_count;
  int taskMsgSize = Mate_Get_Size(sendcount, sendtype);

  int _localRoot;
  if (Mate_isLocalProcessRank(root, currentTask->_globalId)) _localRoot = _runtime->_globalToLocalRankIdMap[root];
  else																				 	             _localRoot = 0;

  Mate_LocalBarrier();
  memcpy((char*)_gather_buffer + taskMsgSize*currentTask->_localId, sendbuf, taskMsgSize);
  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  	{
	   // Determining receiving MPI process ID using the global task map
	   int mate_root = _runtime->_global_rank_map[root];

		 _MPILock.lock();
		 int ret = MPI_Gather(_gather_buffer, sendcount*localTaskCount, sendtype, recvbuf, recvcount*localTaskCount, recvtype, mate_root, MPI_COMM_WORLD);
		 _MPILock.unlock();
		 if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Gather Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();

  return MPI_SUCCESS;
}
