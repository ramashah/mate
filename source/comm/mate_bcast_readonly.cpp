#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Bcast_readonly( void **buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int _localRoot;
  if (Mate_isLocalProcessRank(root, currentTask->_globalId)) _localRoot = _runtime->_globalToLocalRankIdMap[root];
  else																				            	 _localRoot = 0;

  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  {
  	_bcast_buffer = *buffer;
	  int mate_root = _runtime->_global_rank_map[root];
	  int ret = MPI_SUCCESS;

		_MPILock.lock();
		ret = MPI_Bcast(*buffer, count, datatype, mate_root, MPI_COMM_WORLD);
		_MPILock.unlock();
		if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Bcast_readonly Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();
  unsigned long int _vectorSize = Mate_Get_Size(count, datatype);
  if (currentTask->_localId != _localRoot)
  {
  	*buffer = _bcast_buffer;
//  	memcpy(*buffer, _bcast_buffer, _vectorSize);
   }
  Mate_LocalBarrier();

  return 0;
}
