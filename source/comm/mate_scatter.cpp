#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _scatter_buffer;

int Mate_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int localTaskCount = _runtime->_local_rank_count;
  int taskMsgSize = Mate_Get_Size(recvcount, recvtype);

  int _localRoot;
  if (Mate_isLocalProcessRank(root, currentTask->_globalId)) _localRoot = _runtime->_globalToLocalRankIdMap[root];
  else																				            	 _localRoot = 0;

  if (currentTask->_localId == _localRoot)
  	{
    	unsigned long int _vectorSize = taskMsgSize*localTaskCount;
    	_scatter_buffer = (void*) malloc (_vectorSize);

	   // Determining receiving MPI process ID using the global task map
	   int MPI_root = _runtime->_global_rank_map[root];
	   _MPILock.lock();
	   int ret = MPI_Scatter(sendbuf, sendcount*localTaskCount, sendtype, _scatter_buffer, recvcount*localTaskCount, recvtype, MPI_root, MPI_COMM_WORLD);
	   _MPILock.unlock();
	   if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Scatter Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();

  memcpy(recvbuf, (char*)_scatter_buffer + taskMsgSize*currentTask->_localId, taskMsgSize);

  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  {
  	free (_scatter_buffer);
  }

  return 0;
}
