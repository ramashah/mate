#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Op_create(MPI_User_function *user_fn, int commute, MPI_Op *op)
{
	_MPILock.lock();
	int ret = MPI_Op_create(user_fn, commute, op);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Op_create Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}

	return ret;
}
