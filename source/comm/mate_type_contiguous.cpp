#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_contiguous(int count, MPI_Datatype oldtype, MPI_Datatype* newtype)
{
	_MPILock.lock();
	int ret = MPI_Type_contiguous(count, oldtype, newtype);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: Mate_Type_contiguous Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
