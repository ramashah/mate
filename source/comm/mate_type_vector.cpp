#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_vector(int count, int blocklength, int stride, MPI_Datatype oldtype, MPI_Datatype* newtype)
{
	_MPILock.lock();
	int ret = MPI_Type_vector(count, blocklength, stride, oldtype, newtype);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Type_vector Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
