#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_create_struct(int count, const int array_of_blocklengths[], const MPI_Aint array_of_displacements[], const MPI_Datatype array_of_types[], MPI_Datatype *newtype)
{
	_MPILock.lock();
	int ret = MPI_Type_create_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, newtype);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Type_create_struct Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
