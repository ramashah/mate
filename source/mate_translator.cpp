// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
//
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#include "mate_translator.h"

map<string, string> MPIRenameMap;
bool verbose;
int currentGraphId;
vector<MateSourceFile*> fileVector;
map<string, int> _regionStringIDMap;
vector<MateGraphPragma*> matePragmaList;
std::hash<std::string> hasher;

int main(int argc, char* argv[])
{
	// Initializing Variables
	if (verbose) fprintf(stderr, "[MATE] Initializing MATE Variables...\n");
	loadInputFiles(argc, argv);

  // Rename MPI calls to MATE calls
  if (verbose) fprintf(stderr, "[MATE] Detecting and renaming MPI calls...\n");
	ProcessMPICalls();

  // Parsing MATE Pragma Syntax
  if (verbose) fprintf(stderr, "[MATE] Parsing MATE pragmas...\n");
  parseGraphPragmas();

  // Unparsing Translated Files
  if (verbose) fprintf(stderr, "[MATE] Unparsing translated files...\n");
  unparseFiles();
}

void unparseFiles()
{
	for(int i = 0; i < fileVector.size(); i++)
	{
		if (verbose) fprintf(stderr, "[MATE] Unparsing: [%s]...\n", fileVector[i]->_fileName.c_str());

		string fileName = fileVector[i]->_fileName.c_str();

	  // Determining generated rose and mate filenames
	  size_t pos = fileName.find_last_of("/");
	  if(pos != string::npos)  fileName.assign(fileName.begin() + pos + 1, fileName.end());
	  string fileNameStripped;
	  pos = fileName.find_last_of(".");
	  if(pos != string::npos)  fileNameStripped.assign(fileName.begin(), fileName.begin() + pos);
	  else fileNameStripped = fileName;
	  string mateFilename = "mate_" + fileNameStripped + ".cpp";

	  if (verbose) fprintf(stderr, "[MATE] Unparser: Generating file [%s]...\n", mateFilename.c_str());
	  ofstream out(mateFilename.c_str());
	  out << fileVector[i]->_txt;
	  out.close();
  }
}


string removeComments(string& input)
{
	stringstream cleanString;
	int status = 0;

	for(int i = 0; i < input.size()-1; i++)
	{
		if (status == 0) if (input[i] == '"') status = 3;
	  if (status == 0) if (input[i] == '/' && input[i+1] == '/') status = 1;
	  if (status == 0) if (input[i] == '/' && input[i+1] == '*') status = 2;
	  if (status == 1) if (input[i] == '\n') status = 0;
	  if (status == 2) if (input[i] == '*' && input[i+1] == '/') { status = 0; i += 2;}
		if (status == 3) if (input[i] == '"') status = 0;
		if (status == 0 && i < input.size()) cleanString << input[i];
	}

  return cleanString.str();
}

string getPreBlock(stringstream& input)
{
	stringstream preBlock;
	char c;

	while (input.get(c))
	{
		if (c == '{')	return preBlock.str();
		else preBlock << c;
	}

  return preBlock.str();
}

string getBasicBlock(stringstream& input)
{
	stringstream block;
	int bracketLevel = 1;
	char c;

	input.get(c);
	//block << endl << "// Init Graph" << endl << c;
	block << c;
	while (bracketLevel > 0 && input.get(c))
	{
		if (c == '{')	bracketLevel++;
    if (c == '}') bracketLevel--;
    if (bracketLevel > 0) block << c;
	}

  return block.str();
}


void parseGraphBlock(MateGraphPragma* graph)
{
	regex mateForRegex("for\\s*\\(\\s*\\w");
	smatch match;

	graph->_isForLoop = false;
	if(regex_search(graph->_preBlock, match, mateForRegex))
	{
		graph->_isForLoop = true;
		string forBody = graph->_preBlock.substr(match.position(0) + match.length(0) - 1);
		regex forVarsRegex("(.*)(;)(.*)(;)(.*)(\\))(.|\\n)*");

		graph->_forVarDecl  = std::regex_replace(forBody,forVarsRegex,"$1");
		graph->_forCondition = std::regex_replace(forBody,forVarsRegex,"$3");
		graph->_forIncrement = std::regex_replace(forBody,forVarsRegex,"$5");
	}

	regex mateRegionRegex("mate region\\s*\\(\\w+\\)(\\s*depends\\s*\\((\\s*\\w+\\*?\\@?\\s*,)*\\s*\\w+\\*?\\@?\\s*\\))?\\s*");
  stringstream input{graph->_basicBlock};
  stringstream output;
  string line;

	while (getline(input, line))
	{
		smatch match;
		if(regex_search(line, match, mateRegionRegex))
		{
			regex words_regex("\\w+((\\@\\*)|(\\*\\@)|\\*|\\@)?");
			smatch wordMatch;
			vector<string> wordResults;

	    string::const_iterator searchStart( line.cbegin() );
	    while ( regex_search( searchStart, line.cend(), wordMatch, words_regex ) )
	    {
	    	wordResults.push_back(wordMatch[0]);
	      searchStart += wordMatch.position() + wordMatch.length();
	    }

	    //for (int i = 0; i < wordResults.size(); i++)   printf("%s\n", wordResults[i].c_str());

			MateRegionPragma* newRegion = new MateRegionPragma;
			newRegion->_pragmaString = line;
			newRegion->_preBlock = getPreBlock(input);
			newRegion->_basicBlock = getBasicBlock(input);
			graph->_graphRegions.push_back(newRegion);

			// Obtaining Region Name and Id
			newRegion->_regionName = wordResults[3]; // Position 2 is Region Name
			newRegion->_regionId = hasher(newRegion->_regionName);

			// Inserting dependencies dependencies starting as word 4
			for (int i = 5; i < wordResults.size(); i++)
			{
				MateDependency* newDep = new MateDependency;
				newRegion->_regionDeps.push_back(newDep);
				newDep->_regionName = wordResults[i];
				newDep->_delay = 0;
				newDep->_isNeighbor = false;

        if (newDep->_regionName[newDep->_regionName.size()-1] == '*') { newDep->_delay = 1;      newDep->_regionName.resize(newDep->_regionName.size()-1); }
        if (newDep->_regionName[newDep->_regionName.size()-1] == '@') { newDep->_isNeighbor = 1; newDep->_regionName.resize(newDep->_regionName.size()-1); }
        if (newDep->_regionName[newDep->_regionName.size()-1] == '*') { newDep->_delay = 1;      newDep->_regionName.resize(newDep->_regionName.size()-1); }
        if (newDep->_regionName[newDep->_regionName.size()-1] == '@') { newDep->_isNeighbor = 1; newDep->_regionName.resize(newDep->_regionName.size()-1); }

        newDep->_regionId = hasher(newDep->_regionName);
			}
		}
	}
}

void parseGraphPragmas()
{
	regex mateGraphRegex("#pragma\\s+mate\\s+graph");

	for(int i = 0; i < fileVector.size(); i++)
	{
    stringstream input{fileVector[i]->_txt};
    stringstream output;
    string line;

    output << "#include \"mate_runtime.h\"" << endl;
    while (getline(input, line))
    {
    	smatch match;
    	if(regex_search(line, match, mateGraphRegex))
    	{
    		currentGraphId++;
    		MateGraphPragma* newGraph = new MateGraphPragma;
    		matePragmaList.push_back(newGraph);
    		newGraph->_pragmaString = line;
    		newGraph->_preBlock = getPreBlock(input);
    		newGraph->_basicBlock = getBasicBlock(input);

    		parseGraphBlock(newGraph);

    		ostringstream graphName;
    		graphName << currentGraphId << fileVector[i]->_fileName;
    		for (int i = 0; i < newGraph->_graphRegions.size(); i++) graphName << newGraph->_graphRegions[i]->_regionId;
    		size_t graphId = hasher(graphName.str());

    		output << "  //" << newGraph->_pragmaString << endl;
    		output << "  if (Mate_RegisterGraph(" << std::hex << "0x" << graphId << ", ";
    		if (newGraph->_isForLoop)	output << "__MATE_FOR_GRAPH";
    		else output << "__MATE_NORMAL_GRAPH";
    		output << "))" << endl << "  {" << endl;

    		for (int i = 0; i < newGraph->_graphRegions.size(); i++) output << "    Mate_AddRegion(" << std::hex << "0x" << newGraph->_graphRegions[i]->_regionId << ");" << endl;
				output << "    Mate_LocalBarrier();" << endl;

				for(int j = 0; j < newGraph->_graphRegions.size(); j++)
					for(int k = 0; k < newGraph->_graphRegions[j]->_regionDeps.size(); k++)
					{
						if (newGraph->_graphRegions[j]->_regionDeps[k]->_isNeighbor == true)  output << "    Mate_InterRank(";
						if (newGraph->_graphRegions[j]->_regionDeps[k]->_isNeighbor == false) output << "    Mate_AddDependency(";
						output << std::hex << "0x" << newGraph->_graphRegions[j]->_regionId << ", " << "0x" << newGraph->_graphRegions[j]->_regionDeps[k]->_regionId << ", " << newGraph->_graphRegions[j]->_regionDeps[k]->_delay << ");" << endl;
					}

				output << "  }" << endl;

    		if (newGraph->_isForLoop) output << "  " << newGraph->_forVarDecl << ";" << endl;

       	output << "  for(size_t __rId = Mate_GetNextRegionID(); __rId != __ROOT; __rId = Mate_GetNextRegionID()) switch(__rId)" << endl;
    	  output << "  {" << endl;
    	  if (newGraph->_isForLoop) output << "  case __EVAL: " << newGraph->_forIncrement << "; if (!(" << newGraph->_forCondition << ")) Mate_EndForLoop(); " << "break;" << endl;
    	  for (int i = 0; i < newGraph->_graphRegions.size(); i++)
    	  {
    	  	output << endl << "  case " << std::hex << "0x" << newGraph->_graphRegions[i]->_regionId << ": // " << newGraph->_graphRegions[i]->_pragmaString << ":" << endl << "  {" << endl;
          output << newGraph->_graphRegions[i]->_basicBlock << endl;
    	  	output << "  break;" << endl << "  }" << endl;
    	  }
    	  output	<< "}";
    	}
    	else output << line << endl;
    }

    fileVector[i]->_txt = output.str();
	}

	int mainFile = -1;

	// Unparsing Main File
	for(int i = 0; i < fileVector.size(); i++)
	{
		stringstream input{fileVector[i]->_txt};
		stringstream output;
		stringstream newInput;
		string line;

		while (getline(input, line))
		{
			regex mainRegex("(.*)(\\s*)(main)(\\s*)(\\()");

			if(regex_search(line, mainRegex))
			{
				mainFile = i;
				string newLine = std::regex_replace(line, mainRegex,"$1 __mate_execute$5");
				output << newLine << input.rdbuf() << endl;
  			output << "int main(int argc, char **argv)" << endl;
  			output << "{" << endl;
  			output << " Mate::_runtime = new Mate::MateRuntime();" << endl;
  			output << " return Mate::_runtime->initializeMate(argc, argv);" << endl;
  			output << "}" << endl;
			}
			else  output << line << endl;
		}

		fileVector[i]->_txt = output.str();
	}

}

void exitWithError(const char* errorMsg)
{
	 fprintf(stderr, "[Mate] Error: %s\n", errorMsg);
	 exit(-1);
}

string replaceString(string subject, const string& search, const string& replace)
{
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != string::npos)
	{
	 subject.replace(pos, search.length(), replace);
	 pos += replace.length();
	}
	return subject;
}

void loadInputFiles(int argc, char* argv[])
{
	currentGraphId = 0;
  for (int i = 1; i < argc; i++)
  {
  	MateSourceFile* file = new MateSourceFile();
  	file->_fileName = argv[i];
  	// fprintf(stderr, "FileName: %s\n", file->_fileName.c_str());

    // Reading file
    ifstream t(file->_fileName.c_str());
    t.seekg(0, ios::end); size_t size = t.tellg(); t.seekg(0);
    file->_txt.resize(size);
    t.read(&file->_txt[0], size);
    // fprintf(stderr, "%s\n", file->_txt.c_str());

    file->_txt = removeComments(file->_txt);

    fileVector.push_back(file);
  }

  // Setting verbose if defined through the --mate-verbose argument
  verbose = false;
	char* mate_verbose_string = getenv("MATE_VERBOSE_TRANSLATION");
	if (mate_verbose_string != NULL) if(!strcmp(mate_verbose_string, "1")) verbose = true;
}



void ProcessMPICalls()
{
  std::regex MPICallRegex("(MPI)((_[A-Za-z]+)+)\\s*\\(");
  std::regex schedCallRegex("(sched)((_[A-Za-z]+)+)\\s*\\(");
  std::regex pthreadCallRegex("(pthread)((_[A-Za-z]+)+)\\s*\\(");

	for(int i = 0; i < fileVector.size(); i++)
	{
    fileVector[i]->_txt = std::regex_replace(fileVector[i]->_txt,MPICallRegex,"Mate$2(");
    fileVector[i]->_txt = std::regex_replace(fileVector[i]->_txt,schedCallRegex,"Mate_$1$2(");
    fileVector[i]->_txt = std::regex_replace(fileVector[i]->_txt,pthreadCallRegex,"Mate_$1$2(");
  }
}
