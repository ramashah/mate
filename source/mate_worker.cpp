// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

// Mate includes
#include "mate_runtime.h"
#include "mate_worker.h"

void __run(Mate::Worker* thread) {thread->execute();}

namespace Mate
{

void Worker::initialize(int localId, MateRuntime* runtime)
{
	this->_localId = localId;
	this->_runtime = runtime;
	this->_currentRank = NULL;
	this->_timeLapses.reserve(10000);
	this->_timeTaskId.reserve(10000);
}

void Worker::execute()
{
  // Set own affinity with a particular core in the CPU. Affinity is set in contiguous cores regarding the thread id.
	Mate_setAffinity(_affinity);

	pthread_yield();
	_runtime->initBarrier->wait();
	_runtime->initBarrier->wait();

	// Do work until all tasks finish.
	while(_runtime->_local_rank_finished < _runtime->_local_rank_count)
	{
		for (int i = 0; i < _runtime->_local_rank_count; i++)
		if (_runtime->_ranksLock[i].trylock())
		if (_runtime->_ranks[i]._isFinished == false)
		{

			if (_runtime->_ranks[i]._isReady == true)
			{
				_currentRank = &_runtime->_ranks[i];
				_runtime->_ranks[i]._isReady = false;

				if (_runtime->_useMateTiming == true)
				{
					t_end = MPI_Wtime();
					double d_interval = t_end-t_init;
					_timeLapses.push_back(d_interval);
					_timeTaskId.push_back(-1);
					t_init = t_end;
				}

				//printf("Worker %lu - Running: %lu\n", _localId, i);
				_runtime->_ranks[i].resume();
				//printf("Worker %lu - Release: %lu\n", _localId, i);

				if (_runtime->_useMateTiming == true)
				{
					t_end = MPI_Wtime();
					double d_interval = t_end-t_init;
					_timeLapses.push_back(d_interval);
					_timeTaskId.push_back(i);
					t_init = t_end;
				}

				_currentRank = NULL;
				_runtime->_ranksLock[i].unlock();
				 break;
			}

	  if (_runtime->commLock.trylock())
	  {
	   for (int k = 0; k < _runtime->_ranks[i]._graphVector.size(); k++)
	   for (int j = 0; j < _runtime->_ranks[i]._graphVector[k]->_regionVector.size(); j++)
	   if (_runtime->_ranks[i]._graphVector[k]->_regionVector[j]->_MPIRequests.size() > 0)
	   {
	    int flag = false;
	    _MPILock.lock();
	    MPI_Testall(_runtime->_ranks[i]._graphVector[k]->_regionVector[j]->_MPIRequests.size(), _runtime->_ranks[i]._graphVector[k]->_regionVector[j]->_MPIRequests.data(), &flag, MPI_STATUS_IGNORE);
	    _MPILock.unlock();
	    if (flag > 0)
	    {
	     _runtime->_ranks[i]._graphVector[k]->_regionVector[j]->_MPIRequests.clear();
	     _runtime->_ranks[i]._graphVector[k]->_regionVector[j]->_currentStep++;
	    }
	   }
	   _runtime->commLock.unlock();
	  }

			_runtime->_ranks[i]._isReady = _runtime->_ranks[i].isReady();
			_runtime->_ranksLock[i].unlock();
		}
	}

}


}
