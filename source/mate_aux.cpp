// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

// Mate includes
#include "mate_runtime.h"
#include "mate_aux.h"
#include <cstring>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <atomic>

map <int, void*> idxToSHMPointerMap;
void* localShareBuffer;

namespace Mate
{

Lock::Lock()
{
	if(pthread_mutex_init(&_lock, 0))
		printf("[Warning] Lock Initialization Error");
}

Lock::~Lock()
{
	pthread_mutex_destroy(&_lock);
}

void Lock::lock()
{
	pthread_mutex_lock(&_lock);
}

void Lock::unlock()
{
	pthread_mutex_unlock(&_lock);
}

bool Lock::trylock()
{
	return pthread_mutex_trylock(&_lock) == 0;
}

void printAffinity()
{
	 cpu_set_t cpuset;
	 if (pthread_getaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) != 0)  printf("[WARNING] Problem obtaining affinity.\n");
   for (int i = 0; i < CPU_SETSIZE; i++)
	  if (CPU_ISSET(i, &cpuset)) printf("%2d ", i);
}

Barrier::Barrier(int count)
{
	if(pthread_barrier_init(&_barrier, NULL, count)) printf("[Warning] Barrier Initialization Error.\n");
}

Barrier::~Barrier()
{
	pthread_barrier_destroy(&_barrier);
}

void Barrier::wait()
{
	pthread_barrier_wait(&_barrier);
}

void Mate_setAffinity(vector<int> cpuIds)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	for (int i = 0; i < cpuIds.size(); i++) CPU_SET(cpuIds[i], &cpuset);
	if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) != 0) printf("[WARNING] Problem assigning affinity: %d.\n", cpuIds[0]);
}

// This is a helper function that uses the threadID to determine what is the worker is running this task. With that
// we can determine what task this is, by reading the _currentRank field of this worker. There is no other way to
// determine what task ID this is, because the user code is not modified.
Worker* getCurrentWorker()
{
 pthread_t tid = pthread_self();
 Worker* worker = _runtime->_threadIdToWorkerMap[tid];
 return worker;
}

Mate_Rank* getCurrentRank()
{
	return getCurrentWorker()->_currentRank;
}



// Returns true if the currently executing region is the root region. That is, it is outside any graph.
bool isRootRegion(Mate_Rank* currentTask) { return currentTask->_currentRegionId == 0; }

} // namespace Mate

using namespace Mate;

// This is a helper function that replaces current argc and argv with other ones.
void copyArguments(int* destArgc, char*** destArgv, int sourceArgc, char** sourceArgv)
{
	*destArgc = sourceArgc;
    *destArgv = (char**) malloc (sourceArgc * sizeof(char*));
    if (*destArgv == NULL) {fprintf(stderr, "[MATE] Error: Memory Allocation Failure (01)\n"); exit(0);}

    for (int i = 0; i < sourceArgc; i++)
		{
			int size = strlen(sourceArgv[i]) + 1; // +1 for EOS character
			(*destArgv)[i]  = (char*) malloc (sizeof(char)*size);
			if ((*destArgv)[i] == NULL) {fprintf(stderr, "[MATE] Error: Memory Allocation Failure (02)\n"); MPI_Abort(-1, MPI_COMM_WORLD);}
			strcpy((*destArgv)[i], sourceArgv[i]);
		}
}



// This is a helper function that returns whether or not the destination task is local or remote
int isLocalRoot() {	if (getCurrentRank()->_localId == 0) return 1; return 0; }


void Mate_ExitWithError(int code, const char* reason)
{
	fprintf(stderr, "[MATE] Error: 0x%X - %s\n", code, reason);
	exit(code);
}

double Mate_GetTaskScheduleTime(){return getCurrentRank()->_scheduleTime;}
double Mate_GetTaskBarrierTime(){return getCurrentRank()->_barrierTime;}

vector<double>* Mate_GetLocalThreadTimeLapses(int threadId){return &(_runtime->_workers[threadId]._timeLapses);}
vector<int>* Mate_GetLocalThreadTimeTaskIds(int threadId){return &(_runtime->_workers[threadId]._timeTaskId);}

void Mate_GetGlobalThreadScheduleTime(double* scheduleTimes)
{
	double* localWorkerScheduleTimes = (double*) calloc (_runtime->_local_worker_count,sizeof(double));
	for (int i = 0; i < _runtime->_local_worker_count; i++)
	for (int j = 0; j < _runtime->_workers[i]._timeLapses.size(); j++)
	  if (_runtime->_workers[i]._timeTaskId[j] == -1) localWorkerScheduleTimes[i] += _runtime->_workers[i]._timeLapses[j];
	MPI_Allgather(localWorkerScheduleTimes, _runtime->_local_worker_count, MPI_DOUBLE, scheduleTimes, _runtime->_local_worker_count, MPI_DOUBLE, MPI_COMM_WORLD);
};

void Mate_GetGlobalThreadComputeTime(double* computeTimes)
{
	double* localThreadComputeTimes = (double*) calloc (_runtime->_local_worker_count,sizeof(double));
	for (int i = 0; i < _runtime->_local_worker_count; i++)
	for (int j = 0; j < _runtime->_workers[i]._timeLapses.size(); j++)
		if (_runtime->_workers[i]._timeTaskId[j] != -1) localThreadComputeTimes[i] += _runtime->_workers[i]._timeLapses[j];
	MPI_Allgather(localThreadComputeTimes, _runtime->_local_worker_count, MPI_DOUBLE, computeTimes, _runtime->_local_worker_count, MPI_DOUBLE, MPI_COMM_WORLD);
};

void Mate_StartTimers()
{
	double t_current = MPI_Wtime();
	for (int i = 0; i < _runtime->_local_worker_count; i++) { _runtime->_workers[i].t_init = t_current; _runtime->_workers[i].t_end = t_current; }
	_runtime->_useMateTiming = true;
}

extern "C" void mate_starttimers_() {	Mate_StartTimers(); }

void Mate_ResetTimers()
{
	double t_current = MPI_Wtime();
	for (int i = 0; i < _runtime->_local_worker_count; i++)
	{
		_runtime->_workers[i]._timeLapses.clear();
		_runtime->_workers[i]._timeTaskId.clear();
		_runtime->_workers[i].t_init = t_current;
		_runtime->_workers[i].t_end = t_current;
	}
}

void Mate_StopTimers()
{
	_runtime->_useMateTiming = false;
}

extern "C" void mate_stoptimers_() {	Mate_StopTimers(); }

void Mate_local_rank_id(int* localRankId){*localRankId = getCurrentRank()->_localId;}
void Mate_local_rank_count(int* localRankCount){*localRankCount = _runtime->_local_rank_count;}
extern "C" void mate_local_rank_id_(int* localRankId){*localRankId = getCurrentRank()->_localId;}
extern "C" void mate_local_rank_count_(int* localRankCount){*localRankCount = _runtime->_local_rank_count;}
void Mate_local_worker_count(int* localThreadCount){*localThreadCount = _runtime->_local_worker_count;}
void Mate_global_process_id(int *globalProcessId){*globalProcessId = _runtime->_process_id;}
void Mate_global_process_count(int *globalProcessCount){*globalProcessCount = _runtime->_process_count;}
void Mate_global_worker_count(int* globalThreadCount){*globalThreadCount = _runtime->_global_worker_count;};

void Mate_LocalBcast(void* ptr, size_t size, int local_root)
{
	Mate_LocalBarrier();
	Mate_Rank* currentRank = getCurrentRank();

	if (currentRank->_localId == local_root)
	{
	 localShareBuffer = malloc(size);
	 memcpy(localShareBuffer, ptr, size);
	}

	Mate_LocalBarrier();

 memcpy(ptr, localShareBuffer, size);

	Mate_LocalBarrier();
}

bool Mate_isLocalProcessRank(int dest, int current)
{
	return _runtime->_global_rank_map[dest] == _runtime->_global_rank_map[current];
}

extern "C" void mate_islocalprocessrank_(int* dest, int* current, int* res) { *res = Mate_isLocalProcessRank(*dest, *current); }

int Mate_getLocalRankId(int dest) {	return _runtime->_globalToLocalRankIdMap[dest]; }
extern "C" void mate_getlocalrankid_(int* dest, int* localId) {	*localId =  _runtime->_globalToLocalRankIdMap[*dest]; }

void Mate_LocalLock()
{
	_runtime->localLock.lock();
}

void Mate_LocalUnlock()
{
	_runtime->localLock.unlock();
}


void* Mate_GetLocalPointer(int key)
{
	if (idxToSHMPointerMap.find(key) ==  idxToSHMPointerMap.end()) printf("Error: Shared Memory Key not found (%d)\n", key);
  return idxToSHMPointerMap[key];
}

void Mate_AtomicAddDouble(double* ptr, double value)
{
	atomic<double>* atom = (atomic<double>*) ptr;
	*ptr += value;
}

int Mate_sched_setaffinity(pid_t pid, size_t cpusetsize, const cpu_set_t *mask){return 0;};
int Mate_pthread_setaffinity_np(pthread_t thread, size_t cpusetsize, const cpu_set_t *cpuset){return 0;};
int Mate_pthread_setaffinity(pthread_t thread, size_t cpusetsize, const cpu_set_t *cpuset){return 0;};

void Mate_CustomMapping(int* newMap)
{
	Mate_Rank* currentTask = getCurrentRank();

	int localId  = currentTask->_localId;
	int globalId = currentTask->_globalId;

	if (localId == 0)
	{
		int* NewGlobalToLocalRankIdMap = (int*) calloc (sizeof(int), _runtime->_global_rank_count);
		int* NewGlobalRankMap = (int*) calloc (sizeof(int), _runtime->_global_rank_count);

		for (int i = 0; i < _runtime->_global_rank_count; i++)
		{
			NewGlobalToLocalRankIdMap[newMap[i]] = _runtime->_globalToLocalRankIdMap[i];
			NewGlobalRankMap[newMap[i]] = _runtime->_global_rank_map[i];
		}

		_runtime->_globalToLocalRankIdMap = NewGlobalToLocalRankIdMap;
		_runtime->_global_rank_map = NewGlobalRankMap;

		MPI_Barrier(MPI_COMM_WORLD);
	}

	Mate_Barrier(MPI_COMM_WORLD);

	currentTask->_globalId = newMap[currentTask->_globalId];

	Mate_Barrier(MPI_COMM_WORLD);


}
