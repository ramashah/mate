// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#include <stdio.h>
#include "mate_runtime.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

extern int __mate_execute(int argc, char** argv);
using namespace Mate;

namespace Mate
{

// This is the function assigned to each coroutine. This does not execute right away until the resume() function is called.
void __taskWrapper() { _runtime->_currentRank->runTask(); }

void Mate_Rank::runTask()
{
 // Releasing context switch lock
 _runtime->switchLock.unlock();

 // Coming back to runtime to continue initialization
 yield();

	// Creating Root region
	Mate_Region* rootRegion = new Mate_Region;
	rootRegion->_active = true;
	rootRegion->_currentStep = 0;
	rootRegion->_regionId = __ROOT;

	map<size_t, Mate_Graph*> _newGraphMap;
	_newGraphMap[__ROOT] = new Mate_Graph;
	_graphMap.swap(_newGraphMap);
	_graphVector.push_back(_graphMap[__ROOT]);

	map<size_t, Mate_Region*> _newRegionMap;
	_newRegionMap[__ROOT] = rootRegion;
	_regionMap.swap(_newRegionMap);

	_currentGraph = _graphMap[__ROOT];
	_currentGraph->_regionVector.push_back(rootRegion);
	_currentGraph->_activeRegions = 1;
	_currentGraph->_enforceMaxStep = false;
	_currentGraph->_maxStep = 0;
	_currentGraph->_graphType = __MATE_NORMAL_GRAPH;

	// __mate_execute() is the former main() function of the user program
	__mate_execute(_argc, _argv);

	// Once execute finishes, we set the task as done and yield.
	_isFinished = true;

	_runtime->runtimeLock.lock();
	_runtime->_local_rank_finished++;
	_runtime->runtimeLock.unlock();

	// Coming back to runtime to finish execution
	 yield();
}

void Mate_Rank::initialize(int localId)
{
	_localId = localId;

	// We need to do a hard copy of argc and argv for every task because
	// they are supposed to represent independent processes. Otherswise,
	// a logical copy would mean that they all share the same space for
	// the arguments.
	copyArguments(&_argc, &_argv, _runtime->_argc, _runtime->_argv);

	_currentRegionId = __ROOT;
	_scheduleTime = 0;
	_barrierTime = 0;

	// Initializing state flags
	_isFinished = false;
	_isBarrier = false;
	_isReady = true;
}

// This function resumes the execution of the rank.
void Mate_Rank::resume()
{
 _runtime->switchLock.lock();
 _yieldContext = co_active();
 co_switch(_rankContext);
 _runtime->switchLock.unlock();
}

// Yield back to worker
void Mate_Rank::yield()
{
 _runtime->switchLock.lock();
 _rankContext = co_active();
 co_switch(_yieldContext);
 _runtime->switchLock.unlock();
}

bool Mate_Rank::isReady()
{
	if (_isBarrier == true) return false;

	_currentRegionId = getNextRegionID();

	// If there are no more regions left in the graph, exit the graph.
	if (_currentGraph->_activeRegions == 0)
	{
		_currentGraph = _graphMap[__ROOT];
		_currentRegionId = __ROOT;
		return true;
	}

	if (_currentRegionId == __MATE_NO_REGION) return false;
	return true;
}

size_t Mate_Rank::getNextRegionID()
{
	for (int i = 0; i < _currentGraph->_regionVector.size(); i++)
	if (_currentGraph->_regionVector[i]->_active)
	if (_currentGraph->_regionVector[i]->_MPIRequests.size() == 0)
	{
		bool isReady = true;

		// Traverse the dependencies Vector. If any of its dependency regions are in a step
		// smaller than our current step, it means that we still need to wait for it.
	  for(int j = 0; j < _currentGraph->_regionVector[i]->_deps.size(); j++)
	  if (_currentGraph->_regionVector[i]->_deps[j]->_depRegion->_currentStep + _currentGraph->_regionVector[i]->_deps[j]->_depDelay <= _currentGraph->_regionVector[i]->_currentStep)  isReady = false;

	  if (_currentGraph->_graphType == __MATE_FOR_GRAPH)
    	if (_currentGraph->_regionVector[i]->_currentStep ==  _currentGraph->_maxStep && _currentGraph->_enforceMaxStep)
    	{ isReady = false;  _currentGraph->_regionVector[i]->_active = false; _currentGraph->_activeRegions--;}

	  if (isReady)
	  {
	  	if (_currentGraph->_graphType == __MATE_FOR_GRAPH)
	  		if (_currentGraph->_regionVector[i]->_currentStep > _currentGraph->_maxStep)
	  		{_currentGraph->_maxStep = _currentGraph->_regionVector[i]->_currentStep; return __EVAL;}

	  	return _currentGraph->_regionVector[i]->_regionId;
	  }
	}

	return __MATE_NO_REGION;
}


} // namespace Mate

void Mate_AddRegion(size_t regionId)
{
	Mate_Rank* _currentRank = getCurrentRank();
	mateRegionStruct* newRegion = new mateRegionStruct;
	newRegion->_regionId = regionId;
	newRegion->_active = true;
	newRegion->_currentStep = 0;
	_currentRank->_currentGraph->_regionVector.push_back(newRegion);
	_currentRank->_currentGraph->_activeRegions++;
	_currentRank->_regionMap[regionId] = newRegion;
}

void Mate_AddDependency(size_t srcRegionId, size_t depRegionId, int delay)
{
	Mate_Rank* _currentRank = getCurrentRank();

	mateDependencyStruct* newDep = new mateDependencyStruct;
	newDep->_depDelay = delay;
	newDep->_depRegion = _currentRank->_regionMap[depRegionId];
	_currentRank->_regionMap[srcRegionId]->_deps.push_back(newDep);
}

void Mate_InterRank(size_t srcRegionId, size_t depRegionId, int delay)
{
	Mate_Rank* _currentRank = getCurrentRank();

	for (int i = 0; i < _currentRank->_localNeighbors.size(); i++)
	{
		mateDependencyStruct* newDep = new mateDependencyStruct;
		newDep->_depDelay = delay;
		newDep->_depRegion = _runtime->_ranks[_currentRank->_localNeighbors[i]]._regionMap[depRegionId];
		_currentRank->_regionMap[srcRegionId]->_deps.push_back(newDep);
	}
}

bool Mate_RegisterGraph(size_t graphId, int graphType)
{
	Mate_Rank* _currentRank = getCurrentRank();
  bool retVal = false;

	if (_currentRank->_graphMap.find(graphId) == _currentRank->_graphMap.end())
	{
		Mate_Graph* curGraph = new Mate_Graph;
		curGraph->_maxStep = 0;
		_currentRank->_graphMap[graphId] = curGraph;
		_currentRank->_graphVector.push_back(curGraph);
		retVal = true;
	}

	_currentRank->_currentGraph = _currentRank->_graphMap[graphId];
	for (int i = 0; i < _currentRank->_currentGraph->_regionVector.size(); i++)	_currentRank->_currentGraph->_regionVector[i]->_active = true;
	_currentRank->_currentGraph->_graphType = graphType;
	_currentRank->_currentGraph->_activeRegions = _currentRank->_currentGraph->_regionVector.size();
	_currentRank->_currentGraph->_enforceMaxStep = false;

	return retVal;
}

void Mate_AddLocalNeighbor(int localRankId)
{
	Mate_Rank* _currentRank = getCurrentRank();

	if (localRankId < 0 || localRankId >= _runtime->_local_rank_count)
	{
		printf("[MATE] Task %d Error: in Mate_AddLocalNeighbor, Invalid Local Rank Id %d .\n", localRankId);
		MPI_Finalize();
		exit(0);
	};

	_currentRank->_localNeighbors.push_back(localRankId);
}

extern "C" void mate_addlocalneighbor_(int* globalRankId) { Mate_AddLocalNeighbor(*globalRankId); }


void Mate_EndForLoop()
{
	Mate_Rank* _currentRank = getCurrentRank();
	_currentRank->_currentGraph->_enforceMaxStep = true;
}

size_t Mate_GetNextRegionID()
{
	double t_init;
	double t_end;

	if (_runtime->_useMateTiming == true)
	 t_init = MPI_Wtime();

	Mate_Rank* _currentRank = getCurrentRank();
	if (_currentRank->_currentRegionId != __EVAL)
	{
		Mate_Region* curRegion = _currentRank->_regionMap[_currentRank->_currentRegionId];
		if (_currentRank->_currentGraph->_graphType == __MATE_NORMAL_GRAPH) {curRegion->_active = false; _currentRank->_currentGraph->_activeRegions--;}
		// Increase its current step by 1 if no pending MPI requests exist
		if (curRegion->_MPIRequests.size() == 0)	curRegion->_currentStep++;
	}

	bool isTaskReady = _currentRank->isReady();

  if (isTaskReady == false)
 	{
  	if (_runtime->_useMateTiming == true)
  	{
  		t_end = MPI_Wtime();
  		double d_interval = t_end-t_init;
  		_currentRank->_scheduleTime += d_interval;
  	}

  	_currentRank->yield();
 	}

	return _currentRank->_currentRegionId;
}

extern "C" void mate_getnextregionid_(size_t* regionId) { *regionId = Mate_GetNextRegionID(); }
extern "C" void mate_interrank_(size_t srcRegionId, size_t depRegionId, int delay) { Mate_InterRank(srcRegionId, depRegionId, delay); }

