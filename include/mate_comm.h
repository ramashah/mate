// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_COMM_H_
#define MATE_COMM_H_

#include "mpi.h"
#include <vector>
#include <map>

#define NOSTEP -1

using namespace std;

extern int MATE_MAX_TAG;
extern int MATE_MAX_LOCALTASKS;

extern bool firstLocalTimeTaken;
extern double firstLocalTime;
extern double lastLocalTime;

extern void* _reduce_accumulator;
extern void* _local_allreduce_accumulator;
extern void* _allreduce_accumulator;
extern unsigned long int _reduce_accumulator_size;
extern unsigned long int _allreduce_accumulator_size;
extern bool _initializedReduce;

extern void* _bcast_buffer;
extern void* _scatter_buffer;
extern void* _gather_buffer;
extern void* _Allgather_sendbuffer;
extern void* _Allgather_recvbuffer;
extern void* _Allgatherv_sendbuffer;
extern void* _Allgatherv_recvbuffer;

extern unsigned long int _Allgather_sendbuffer_size;
extern unsigned long int _Allgatherv_sendbuffer_size;
extern unsigned long int _AllgathervRecvVectorSize;
extern unsigned long int _AllgathervSendVectorSize;
extern vector<int> _newAllgathervRecvCounts;
extern vector<int> _newAllgathervDispCounts;
extern int _barrierCount;
extern bool _isGlobalBarrier;


#define MATE_INITIAL_BUFFER_ALLOCATION_SIZE 409600


// Helper functions
size_t Mate_Get_Size(int count, MPI_Datatype datatype);

// MPI emulation Functions
int Mate_Init(int* argc, char*** argv);
int Mate_Finalize();
int Mate_Barrier(MPI_Comm comm);
int Mate_LocalBarrier();
int Mate_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
int Mate_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm);
int Mate_Allgather(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);
int Mate_Allgatherv(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, const int *recvcounts, const int *displs, MPI_Datatype recvtype, MPI_Comm comm);
int Mate_Allreduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
int Mate_Reduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,  MPI_Op op, int root, MPI_Comm comm);
int Mate_Bcast( void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);
int Mate_Bcast_readonly(void **buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);
int Mate_Send(const void* buffer, int size, MPI_Datatype datatype,  int destProcess, int destRank, int tag, MPI_Comm comm, MPI_Request* request);
int Mate_Recv(void* buffer, int size, MPI_Datatype datatype, int sourceProcess, int sourceRank, int tag, MPI_Comm comm, MPI_Request* request);
int Mate_Comm_split(MPI_Comm comm, int color, int key, MPI_Comm *newcomm);
int Mate_Abort(MPI_Comm comm, int errorcode);
double Mate_Wtime();
int Mate_Address(void *location, MPI_Aint *address);
int Mate_Get_count( const MPI_Status *status, MPI_Datatype datatype, int *count);
int Mate_Op_create(MPI_User_function *user_fn, int commute, MPI_Op *op);
void Mate_OpExec(void *a, const void *b, int count, MPI_Datatype datatype, MPI_Op op);
int Mate_Type_contiguous(int count, MPI_Datatype oldtype, MPI_Datatype* newtype);
int Mate_Type_vector(int count, int blocklength, int stride, MPI_Datatype oldtype, MPI_Datatype* newtype);
int Mate_Type_struct(int count, int *array_of_blocklengths, MPI_Aint *array_of_displacements, MPI_Datatype *array_of_types, MPI_Datatype *newtype);
int Mate_Type_create_struct(int count, const int array_of_blocklengths[], const MPI_Aint array_of_displacements[], const MPI_Datatype array_of_types[], MPI_Datatype *newtype);
int Mate_Type_commit(MPI_Datatype* type);
int Mate_Type_indexed(int count, int *array_of_blocklengths, int *array_of_displacements, MPI_Datatype oldtype, MPI_Datatype *newtype);
int Mate_Type_free(MPI_Datatype* type);
int Mate_Type_size(MPI_Datatype type, int* size);
int Mate_Pack(const void* source, int count, MPI_Datatype mpiType, void* dest, int size, int* position, MPI_Comm comm);
int Mate_Unpack(const void* source, int size, int* position, void* dest, int count, MPI_Datatype mpiType, MPI_Comm comm);
void Mate_Disable_Remote_Comm();
void Mate_Enable_Remote_Comm();
void Mate_Disable_Local_Comm();
void Mate_Enable_Local_Comm();
void Mate_SetFirstLocalTime();
void Mate_SetLastLocalTime();
double Mate_GetProcessTime();

#endif // MATE_COMM_H_



