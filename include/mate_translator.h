#ifndef MATE_TRANSLATOR_H_
#define MATE_TRANSLATOR_H_

#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <functional>
#include <regex>

using namespace std;

class MateSourceFile
{
	public:

	string _fileName;
	string _txt;
};


class MateDependency
{
	public:
	size_t _regionId;
	string _regionName;
	int _delay;
	bool _isNeighbor;
};

class MateRegionPragma
{
	public:

	size_t _regionId;
	string _regionName;
	string _pragmaString;
	string _preBlock;
	string _basicBlock;
	vector<MateDependency*> _regionDeps;
};

class MateGraphPragma
{
	public:

	string _pragmaString;
	string _preBlock;
	string _basicBlock;
	bool _isForLoop;
	bool _hasVarDef;
	string _forVarDecl;
	string _forCondition;
	string _forIncrement;
	vector<MateRegionPragma*> _graphRegions;
};



string getPreBlock(stringstream& input);
string getBasicBlock(stringstream& input);
void parseRegionPragmas(MateGraphPragma* graph);
void parseGraphBlock(MateGraphPragma* graph);
void parseGraphPragmas();
void ProcessMPICalls();
void unparseFiles();
void findMATERemappingFunction();
void loadInputFiles(int argc, char* argv[]);
void detectMainFunction();
void commitDelayedTransformations();
void exitWithError(const char* errorMsg);
string replaceString(string subject, const string& search, const string& replace);

#endif
