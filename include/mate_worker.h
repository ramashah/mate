// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_WORKER_H_
#define MATE_WORKER_H_

#include "mate_rank.h"
#include "mate_comm.h"
#include "mate_mpi.h"

namespace Mate
{

class Mate_Rank;
class MateRuntime;

class Worker
{
public:

	int _globalId;
	int _localId;

	Mate_Rank* _currentRank;
	MateRuntime* _runtime;
	pthread_t _thread;

	vector<double> _timeLapses;
	vector<int> _timeTaskId;

	double t_init;
	double t_end;

	void initialize(int localId, MateRuntime* runtime);
	void execute();
	void set_affinity();
	vector<int> _affinity;

	Worker(){};
   ~Worker(){};
};


} // namespace Mate

void __run(Mate::Worker* worker);

#endif // MATE_WORKER_H_
