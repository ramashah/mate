runCommand=mpirun
export | grep CRAY_MPICH > /dev/null 2>&1
if [ $? -eq 0 ]
then
  runCommand=srun
fi

echo "$runCommand -n 12 ./jacobiMPI -n 384 -px 1 -py 3 -pz 4 -i 30"
echo "$runCommand -n 1 ./jacobiMATE --mate-ranks 24 -n 384 -px 1 -py 1 -pz 1 -lx 1 -ly 4 -lz 6 -i 30"
