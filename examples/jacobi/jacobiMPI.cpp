#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include <fstream>
#include <vector>
#include <sched.h>

using namespace std;

const int BLOCKZ=64;
const int BLOCKY=64;

enum commType {
	  REMOTE=0,
    LOCAL,
		BOUNDARY
};

typedef struct positionStruct {
	int x;
	int y;
	int z;
} Position;

typedef struct NeighborStruct {
	commType type;
	int globalRankId;
	int globalProcessId;
} Neighbor;


int main(int carg, char* argv[])
{
	int globalProcessId = 0;
	int globalProcessCount = 1;
 int myRank, globalRankCount;

	MPI_Init(&carg, &argv);
 MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
 MPI_Comm_size(MPI_COMM_WORLD, &globalRankCount);

 globalProcessId = myRank;
 globalProcessCount = globalRankCount;

	bool isMainRank = globalProcessId == 0;
	int gDepth = 2; double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };
	// int gDepth = 1; double stencil_coeff[] = { -6.0/12.0, 1.0/12.0 };

	int N = 128;
	int nx, ny, nz;
	int nIters=100;
 int count = 1;
 bool localcomm = true;
 bool useTiming = true;
 double packCount = 1;
	int px=1, py=1, pz=1;
	int rankx, ranky, rankz;
 Neighbor East, West, North, South, Up, Down;
	Position globalProcess;
	Position size;
 Position start;
 Position end;

	MPI_Datatype posType;
	int posLength = 3; MPI_Aint posDispl = 0; MPI_Datatype oldposType = MPI_INT;
	MPI_Type_struct(1, &posLength, &posDispl, &oldposType, &posType);
	MPI_Type_commit(&posType);

 int nMessages = 1;
 int sizeMultiplier = 1;
 int sizeDivider = 1;

 for (int i = 0; i < carg; i++)
 {
  if(!strcmp(argv[i], "-px")) px = atoi(argv[++i]);
  if(!strcmp(argv[i], "-py")) py = atoi(argv[++i]);
  if(!strcmp(argv[i], "-pz")) pz = atoi(argv[++i]);
  if(!strcmp(argv[i], "-n"))  N = atoi(argv[++i]);
  if(!strcmp(argv[i], "-i"))  nIters = atoi(argv[++i]);
  if(!strcmp(argv[i], "-nc")) count = 0;
  if(!strcmp(argv[i], "-np")) packCount = 0;
  if(!strcmp(argv[i], "-nl")) localcomm = false;
  if(!strcmp(argv[i], "-nt")) useTiming = false;
  if(!strcmp(argv[i], "-xm")) nMessages = atoi(argv[++i]);
  if(!strcmp(argv[i], "-xs")) sizeMultiplier = atoi(argv[++i]);
  if(!strcmp(argv[i], "-xd")) sizeDivider = atoi(argv[++i]);
	}

  int mx = px, my = py, mz = pz;

  for (int i = 0; i < carg; i++) {
  if(!strcmp(argv[i], "-mx")) mx = atoi(argv[++i]);
  if(!strcmp(argv[i], "-my")) my = atoi(argv[++i]);
  if(!strcmp(argv[i], "-mz")) mz = atoi(argv[++i]);
	}

	if (px * py * pz != globalProcessCount) { if (isMainRank) printf("[Error] The specified px/py/pz geometry does not match the number of MPI processes (-n %d).\n", globalProcessCount); MPI_Finalize(); return 0; }

	if(N % px > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by px (%d)\n", N, px); MPI_Finalize(); return 0; }
	if(N % py > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by py (%d)\n", N, py); MPI_Finalize(); return 0; }
	if(N % pz > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by pz (%d)\n", N, pz); MPI_Finalize(); return 0; }

	nx = N / px;
	ny = N / py;
	nz = N / pz;

 double *U, *Un, *b;

 int fx = nx + 2 * gDepth;
 int fy = ny + 2 * gDepth;
 int fz = nz + 2 * gDepth;

	U  = (double *)calloc(sizeof(double),fx*fy*fz);
	Un = (double *)calloc(sizeof(double),fx*fy*fz);

 int* globalRankX = (int*) calloc(sizeof(int),globalProcessCount);
 int* globalRankY = (int*) calloc(sizeof(int),globalProcessCount);
 int* globalRankZ = (int*) calloc(sizeof(int),globalProcessCount);
 int*** globalProcessMapping = (int***) calloc(sizeof(int**),pz);
 for (int i = 0; i < pz; i++) globalProcessMapping[i] = (int**) calloc (sizeof(int*),py);
 for (int i = 0; i < pz; i++) for (int j = 0; j < py; j++) globalProcessMapping[i][j] = (int*) calloc (sizeof(int),px);

 int subMapCountX = px / mx;
 int subMapCountY = py / my;
 int subMapCountZ = pz / mz;
 int currentRank = 0;

 for (int sz = 0; sz < subMapCountZ; sz++)
 for (int sy = 0; sy < subMapCountY; sy++)
	for (int sx = 0; sx < subMapCountX; sx++)
	for (int z = sz * mz; z < (sz+1) * mz; z++)
	for (int y = sy * my; y < (sy+1) * my; y++)
	for (int x = sx * mx; x < (sx+1) * mx; x++)
	{ globalRankZ[currentRank] = z; globalRankX[currentRank] = x; globalRankY[currentRank] = y; globalProcessMapping[z][y][x] = currentRank; currentRank++; }

	start.x = gDepth;
	start.y = gDepth;
	start.z = gDepth;
	end.x = start.x + nx;
	end.y = start.y + ny;
	end.z = start.z + nz;

 globalProcess.z = globalRankZ[globalProcessId];
 globalProcess.y = globalRankY[globalProcessId];
 globalProcess.x = globalRankX[globalProcessId];

	West.type  = REMOTE; West.globalProcessId  = globalProcessId;
	East.type  = REMOTE; East.globalProcessId  = globalProcessId;
	North.type = REMOTE; North.globalProcessId = globalProcessId;
	South.type = REMOTE; South.globalProcessId = globalProcessId;
	Up.type    = REMOTE; Up.globalProcessId    = globalProcessId;
	Down.type  = REMOTE; Down.globalProcessId  = globalProcessId;

	Position* globalProcessArray = (Position*) calloc (sizeof(Position) , globalRankCount);
	MPI_Allgather(&globalProcess, 1, posType, globalProcessArray, 1, posType, MPI_COMM_WORLD);

	int* rankProcessArray = (int*) calloc (sizeof(int) , globalRankCount);
	MPI_Allgather(&globalProcessId, 1, MPI_INT, rankProcessArray, 1, MPI_INT, MPI_COMM_WORLD);

	if (globalProcess.x == 0)    West.type  = BOUNDARY;
	if (globalProcess.y == 0)    North.type = BOUNDARY;
	if (globalProcess.z == 0)    Up.type    = BOUNDARY;
	if (globalProcess.x == px-1) East.type  = BOUNDARY;
	if (globalProcess.y == py-1) South.type = BOUNDARY;
	if (globalProcess.z == pz-1) Down.type  = BOUNDARY;

	if(West.type  == REMOTE) West.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x-1];
	if(East.type  == REMOTE) East.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x+1];
	if(North.type == REMOTE) North.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y-1][globalProcess.x];
	if(South.type == REMOTE) South.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y+1][globalProcess.x];
	if(Up.type    == REMOTE) Up.globalProcessId    = globalProcessMapping[globalProcess.z-1][globalProcess.y][globalProcess.x];
	if(Down.type  == REMOTE) Down.globalProcessId  = globalProcessMapping[globalProcess.z+1][globalProcess.y][globalProcess.x];

	for (int i = 0; i < globalRankCount; i++)
	{
  if (West.globalProcessId  == rankProcessArray[i]) West.globalRankId = i;
  if (East.globalProcessId  == rankProcessArray[i]) East.globalRankId = i;
  if (North.globalProcessId == rankProcessArray[i]) North.globalRankId = i;
  if (South.globalProcessId == rankProcessArray[i]) South.globalRankId = i;
  if (Up.globalProcessId    == rankProcessArray[i]) Up.globalRankId = i;
  if (Down.globalProcessId  == rankProcessArray[i]) Down.globalRankId = i;
	}

	free(globalProcessMapping);
	free(globalProcessArray);
	free(rankProcessArray);

 size.x = nx;  size.y = ny; size.z = nz;
	int southTAG=1, northTAG=2, eastTAG=3, westTAG=4, downTAG=5, upTAG=6;

 MPI_Datatype faceXX_type;
 MPI_Datatype faceY_type;
 MPI_Datatype faceX_type;
 MPI_Datatype faceZ_type;

 MPI_Type_vector(size.y, 1, fx, MPI_DOUBLE, &faceX_type); MPI_Type_commit(&faceX_type);
 int* counts = (int*) calloc (sizeof(int) , size.z);
 MPI_Aint* displs = (MPI_Aint*) calloc (sizeof(MPI_Aint) , size.z);
 MPI_Datatype* types = (MPI_Datatype*) calloc (sizeof(MPI_Datatype) , size.z);
 for (int i = 0; i < size.z; i++) { counts[i] = 1;  displs[i] = fy*fx*sizeof(double)*i; types[i] = faceX_type; }

 MPI_Type_struct(size.z / sizeDivider, counts, displs, types, &faceXX_type); MPI_Type_commit(&faceXX_type);
 MPI_Type_vector(size.z / sizeDivider, size.x, fx*fy, MPI_DOUBLE, &faceY_type); MPI_Type_commit(&faceY_type);
 MPI_Type_vector(size.y / sizeDivider, size.x, fx, MPI_DOUBLE, &faceZ_type); MPI_Type_commit(&faceZ_type);

	int count_down  = Down.type  == REMOTE ? count : 0;
	int count_up    = Up.type    == REMOTE ? count : 0;
	int count_east  = East.type  == REMOTE ? count : 0;
	int count_west  = West.type  == REMOTE ? count : 0;
	int count_north = North.type == REMOTE ? count : 0;
	int count_south = South.type == REMOTE ? count : 0;

	for (int k = start.z-gDepth; k < end.z+gDepth; k++)
	for (int j = start.y-gDepth; j < end.y+gDepth; j++)
	for (int i = start.x-gDepth; i < end.x+gDepth; i++)
		 U[k*fy*fx + j*fx + i] = 1;


	if (West.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + d] = 0;
	if (North.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + d*fx + i] = 0;
	if (Up.type    == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[d*fx*fy + j*fx + i] = 0;
	if (East.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + (nx+gDepth+d)] = 0;
	if (South.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + (ny+gDepth+d)*fx + i] = 0;
	if (Down.type  == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[(nz+gDepth+d)*fx*fy + j*fx + i] = 0;

 int request_count = 0;
 int pos = 0;

 int* countFaceX = (int*) calloc (sizeof(int), nMessages);
 int* countFaceY = (int*) calloc (sizeof(int), nMessages);
 int* countFaceZ = (int*) calloc (sizeof(int), nMessages);
 int* offsetFaceX = (int*) calloc (sizeof(int), nMessages);
 int* offsetFaceY = (int*) calloc (sizeof(int), nMessages);
 int* offsetFaceZ = (int*) calloc (sizeof(int), nMessages);

 int bufferSizeX = sizeMultiplier*size.y*size.z;
 int bufferSizeY = sizeMultiplier*size.x*size.z;
 int bufferSizeZ = sizeMultiplier*size.x*size.y;

	bufferSizeX = bufferSizeX/sizeDivider;
	bufferSizeY = bufferSizeY/sizeDivider;
	bufferSizeZ = bufferSizeZ/sizeDivider;

 int msgSizeX = bufferSizeX / nMessages;
 int msgSizeY = bufferSizeY / nMessages;
 int msgSizeZ = bufferSizeZ / nMessages;

	countFaceX[0] = msgSizeX;
	countFaceY[0] = msgSizeY;
	countFaceZ[0] = msgSizeZ;

	offsetFaceX[0] = 0;
	offsetFaceY[0] = 0;
	offsetFaceZ[0] = 0;

 for (int i = 1; i < nMessages; i++)
 {
 	countFaceX[i] = msgSizeX;
 	countFaceY[i] = msgSizeY;
 	countFaceZ[i] = msgSizeZ;

 	offsetFaceX[i] = offsetFaceX[i-1] + msgSizeX;
  offsetFaceY[i] = offsetFaceY[i-1] + msgSizeY;
  offsetFaceZ[i] = offsetFaceZ[i-1] + msgSizeZ;
 }

	countFaceX[nMessages-1] = bufferSizeX - offsetFaceX[nMessages-1];
	countFaceY[nMessages-1] = bufferSizeY - offsetFaceY[nMessages-1];
	countFaceZ[nMessages-1] = bufferSizeZ - offsetFaceZ[nMessages-1];

 double** upSendBuffer    = (double**) calloc (sizeof(double*),gDepth);
 double** downSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** eastSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** westSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** northSendBuffer = (double**) calloc (sizeof(double*),gDepth);
 double** southSendBuffer = (double**) calloc (sizeof(double*),gDepth);
 double** upRecvBuffer    = (double**) calloc (sizeof(double*),gDepth);
 double** downRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** eastRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** westRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
 double** northRecvBuffer = (double**) calloc (sizeof(double*),gDepth);
 double** southRecvBuffer = (double**) calloc (sizeof(double*),gDepth);

 for (int i = 0; i < gDepth; i++)
 {
		upSendBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
		downSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
		eastSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		westSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		northSendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		southSendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		upRecvBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
		downRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
		eastRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		westRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		northRecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		southRecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
 }

	MPI_Request request[24*gDepth*nMessages];

 MPI_Barrier(MPI_COMM_WORLD);
 double computeTime = 0;
 double packTime = 0;
 double unpackTime = 0;
 double sendTime = 0;
 double recvTime = 0;
 double waitTime = 0;
 double execTime = -MPI_Wtime();
	double t_init;
	double t_end;

 MPI_Barrier(MPI_COMM_WORLD);

	for (int iter=0; iter<nIters; iter++)
	{

		if (useTiming) t_init = MPI_Wtime();

		request_count =0;
		for (int k0 = start.z; k0 < end.z; k0+=BLOCKZ) {
			int k1= k0+BLOCKZ<end.z?k0+BLOCKZ:end.z;
			for (int j0 = start.y; j0 < end.y; j0+=BLOCKY) {
				int j1= j0+BLOCKY<end.y?j0+BLOCKY:end.y;
				for (int k = k0; k < k1; k++) {
					 for (int j = j0; j < j1; j++){
							#pragma vector aligned
							#pragma ivdep
							for (int i = start.x; i < end.x; i++)
							{
								double sum = 0;
								sum += U[fx*fy*k     + fx*j         + i] * stencil_coeff[0]; // Central
								for (int d = 1; d <= gDepth; d++)
								{
									double partial_sum = 0;
									partial_sum += U[fx*fy*(k-d) + fx*j         + i]; // Up
									partial_sum += U[fx*fy*(k+d) + fx*j         + i]; // Down
									partial_sum += U[fx*fy*k     + fx*j     - d + i]; // East
									partial_sum += U[fx*fy*k     + fx*j     + d + i]; // West
									partial_sum += U[fx*fy*k     + fx*(j+d)     + i]; // North
									partial_sum += U[fx*fy*k     + fx*(j-d)     + i]; // South
									sum += partial_sum * stencil_coeff[d];
								}
								Un[fx*fy*k     + fx*j + i] = sum; // Update
							}
					 }
				}
			}
		}
		double *temp = NULL;
		temp = U;
		U = Un;
		Un = temp;

		if (useTiming) { t_end = MPI_Wtime();	computeTime += double(t_end-t_init);	}

		if (useTiming) t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(downRecvBuffer[d]  + offsetFaceZ[i], count_down*countFaceZ[i],  MPI_DOUBLE, Down.globalRankId,  upTAG,    MPI_COMM_WORLD, &request[request_count++]);
		if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(upRecvBuffer[d]    + offsetFaceZ[i], count_up*countFaceZ[i],    MPI_DOUBLE, Up.globalRankId,    downTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(eastRecvBuffer[d]  + offsetFaceX[i], count_east*countFaceX[i],  MPI_DOUBLE, East.globalRankId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(westRecvBuffer[d]  + offsetFaceX[i], count_west*countFaceX[i],  MPI_DOUBLE, West.globalRankId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(northRecvBuffer[d] + offsetFaceY[i], count_north*countFaceY[i], MPI_DOUBLE, North.globalRankId, southTAG, MPI_COMM_WORLD, &request[request_count++]);
		if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(southRecvBuffer[d] + offsetFaceY[i], count_south*countFaceY[i], MPI_DOUBLE, South.globalRankId, northTAG, MPI_COMM_WORLD, &request[request_count++]);

		if (useTiming)	 { t_end = MPI_Wtime();	 recvTime += double(t_end-t_init); }

		if (useTiming) t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(end.z-gDepth+d) + fx*start.y            + start.x              ],   packCount*count_down,  faceZ_type,  downSendBuffer[d],    sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(start.z+d)      + fx*start.y            + start.x              ],   packCount*count_up,    faceZ_type,  upSendBuffer[d],  sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (end.x-gDepth+d)     ],   packCount*count_east,  faceXX_type, eastSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (start.x+d)          ],   packCount*count_west,  faceXX_type, westSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(start.y+d)        + start.x              ],   packCount*count_north, faceY_type,  northSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(end.y-gDepth+d)   + start.x              ],   packCount*count_south, faceY_type,  southSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}

		if (useTiming) { t_end = MPI_Wtime(); packTime += double(t_end-t_init); }

		if (useTiming) t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(downSendBuffer[d]  + offsetFaceZ[i],  count_down*countFaceZ[i],  MPI_DOUBLE,  Down.globalRankId,  downTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(upSendBuffer[d]    + offsetFaceZ[i],  count_up*countFaceZ[i],    MPI_DOUBLE,  Up.globalRankId,    upTAG,    MPI_COMM_WORLD, &request[request_count++]);
		if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(eastSendBuffer[d]  + offsetFaceX[i],  count_east*countFaceX[i],  MPI_DOUBLE,  East.globalRankId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(westSendBuffer[d]  + offsetFaceX[i],  count_west*countFaceX[i],  MPI_DOUBLE,  West.globalRankId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
		if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(northSendBuffer[d] + offsetFaceY[i],  count_north*countFaceY[i], MPI_DOUBLE,  North.globalRankId, northTAG, MPI_COMM_WORLD, &request[request_count++]);
		if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(southSendBuffer[d] + offsetFaceY[i],  count_south*countFaceY[i], MPI_DOUBLE,  South.globalRankId, southTAG, MPI_COMM_WORLD, &request[request_count++]);

		if (useTiming) { t_end = MPI_Wtime();	 sendTime += double(t_end-t_init); }

		if (useTiming)  t_init = MPI_Wtime();

		MPI_Waitall(request_count, request, MPI_STATUS_IGNORE);

		 if (useTiming)
		 {
			 t_end = MPI_Wtime();
			 waitTime += t_end-t_init;
		 }

		if (useTiming) t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(downRecvBuffer[d],  packCount*count_down*sizeof(double)*size.y*size.x,  &pos, &U[fx*fy*(end.z+d) + fx*start.y   + start.x    ], packCount*count_down,  faceZ_type, MPI_COMM_WORLD); pos = 0;}
		if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(upRecvBuffer[d],    packCount*count_up*sizeof(double)*size.y*size.x,    &pos, &U[fx*fy*d         + fx*start.y   + start.x    ], packCount*count_up,    faceZ_type,  MPI_COMM_WORLD); pos = 0;}
		if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(eastRecvBuffer[d],  packCount*count_east*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y   + end.x + d  ], packCount*count_east,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(westRecvBuffer[d],  packCount*count_west*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y   + d          ], packCount*count_west,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(northRecvBuffer[d], packCount*count_north*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*d         + start.x    ], packCount*count_north, faceY_type, MPI_COMM_WORLD); pos = 0;}
		if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(southRecvBuffer[d], packCount*count_south*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*(end.y+d) + start.x    ], packCount*count_south, faceY_type, MPI_COMM_WORLD); pos = 0;}

		if (useTiming)	 { t_end = MPI_Wtime();	 unpackTime += double(t_end-t_init); }
 }

	MPI_Barrier(MPI_COMM_WORLD);
	execTime += MPI_Wtime();

 double res = 0;
 double err = 0;
 for (int k=start.z; k<end.z; k++)
	for (int j=start.y; j<end.y; j++)
	for (int i=start.x; i<end.x; i++)
  { double r = U[k*fy*fx + j*fx + i];  err += r * r; }
 MPI_Reduce (&err, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

 double meanComputeTime = 0, sumComputeTime = 0;
 double meanPackTime = 0, sumPackTime = 0;
 double meanUnpackTime = 0, sumUnpackTime = 0;
 double meanSendTime = 0, sumSendTime = 0;
 double meanRecvTime = 0, sumRecvTime = 0;
 double meanWaitTime = 0, sumWaitTime = 0;

 MPI_Reduce(&computeTime, &sumComputeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&packTime, &sumPackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&unpackTime, &sumUnpackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&sendTime, &sumSendTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&recvTime, &sumRecvTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&waitTime, &sumWaitTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

 meanComputeTime = sumComputeTime / globalRankCount;
 meanPackTime = sumPackTime / globalRankCount;
 meanUnpackTime = sumUnpackTime / globalRankCount;
 meanSendTime = sumSendTime / globalRankCount;
 meanRecvTime = sumRecvTime / globalRankCount;
 meanWaitTime = sumWaitTime / globalRankCount;

 double totalTime = meanComputeTime + meanRecvTime + meanPackTime + meanSendTime + meanWaitTime + meanUnpackTime;

 if (useTiming == false) totalTime = execTime;

	if(myRank ==0)
	{
  double gflops = nIters*(double)N*(double)N*(double)N*(2 + gDepth * 8)/(1.0e9);
  printf("Compute:     %.4f\n", meanComputeTime);
  printf("MPI_Irecv:   %.4f\n", meanRecvTime);
  printf("MPI_Isend:   %.4f\n", meanSendTime);
  printf("MPI_Pack:    %.4f\n", meanPackTime);
  printf("MPI_Unpack:  %.4f\n", meanUnpackTime);
  printf("MPI_Waitall: %.4f\n", meanWaitTime);
  printf("%.4fs, %.3f GFlop/s (L2 Norm: %.10g)\n", totalTime, gflops/totalTime, res);
	}

 MPI_Finalize(); return 0;
}
