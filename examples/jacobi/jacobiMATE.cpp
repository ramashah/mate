#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

const int BLOCKZ=96;
const int BLOCKY=64;

enum commType {
	  REMOTE=0,
    LOCAL,
		BOUNDARY
};

typedef struct positionStruct {
	int x;
	int y;
	int z;
} Position;

typedef struct NeighborStruct {
	commType type;
 int processId;
	int localId;
	Position localRank;
} Neighbor;


int main(int carg, char* argv[])
{
 int processId = 0;
 int localId = 0;
	int localRankCount = 1;
	int processCount = 1;

	Mate_Init(&carg, &argv);

	Mate_local_rank_id(&localId);
	Mate_local_rank_count(&localRankCount);
	Mate_global_process_id(&processId);
	Mate_global_process_count(&processCount);

	int globalRankCount = processCount * localRankCount;

	bool isMainRank = localId == 0 && processId == 0;

	int gDepth = 2; double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };
	// int gDepth = 1; double stencil_coeff[] = { -6.0/12.0, 1.0/12.0 };

	int N = 128;
	int nx, ny, nz;
	int nIters=100;
 int count = 1;
	int px=1, py=1, pz=1, lx=1, ly=1, lz=1;
 double packCount = 1;
	int rankx, ranky, rankz;
 Neighbor East, West, North, South, Up, Down;
 Position localRank;
	Position globalProcess;
	Position size;
 Position start;
 Position end;

	MPI_Datatype posType;
	int posLength = 3; MPI_Aint posDispl = 0; MPI_Datatype oldposType = MPI_INT;
	MPI_Type_struct(1, &posLength, &posDispl, &oldposType, &posType);
	MPI_Type_commit(&posType);

 int nMessages = 1;
 int sizeMultiplier = 1;
 int sizeDivider = 1;

 for (int i = 0; i < carg; i++)
 {
  if(!strcmp(argv[i], "-px")) px = atoi(argv[++i]);
  if(!strcmp(argv[i], "-py")) py = atoi(argv[++i]);
  if(!strcmp(argv[i], "-pz")) pz = atoi(argv[++i]);
  if(!strcmp(argv[i], "-lx")) lx = atoi(argv[++i]);
  if(!strcmp(argv[i], "-ly")) ly = atoi(argv[++i]);
  if(!strcmp(argv[i], "-lz")) lz = atoi(argv[++i]);
  if(!strcmp(argv[i], "-n"))  N  = atoi(argv[++i]);
  if(!strcmp(argv[i], "-i"))  nIters = atoi(argv[++i]);
  if(!strcmp(argv[i], "-nc")) count = 0;
  if(!strcmp(argv[i], "-np")) packCount = 0;
  if(!strcmp(argv[i], "-xm")) nMessages = atoi(argv[++i]);
  if(!strcmp(argv[i], "-xs")) sizeMultiplier = atoi(argv[++i]);
  if(!strcmp(argv[i], "-xd")) sizeDivider = atoi(argv[++i]);
	}

 int mx = px, my = py, mz = pz;

 for (int i = 0; i < carg; i++)
 {
  if(!strcmp(argv[i], "-mx")) mx = atoi(argv[++i]);
  if(!strcmp(argv[i], "-my")) my = atoi(argv[++i]);
  if(!strcmp(argv[i], "-mz")) mz = atoi(argv[++i]);
	}

	if (px * py * pz != processCount) { if (isMainRank) printf("[Error] The specified px/py/pz geometry does not match the number of MATE processes (-n %d).\n", processCount); MPI_Finalize(); return 0; }
	if (lx * ly * lz != localRankCount) { if (isMainRank) printf("[Error] The specified lx/ly/lz geometry does not match the number of local ranks (--mate-ranks %d).\n", localRankCount); MPI_Finalize(); return 0;	}

	if(N % px > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by px (%d)\n", N, px); MPI_Finalize(); return 0; }
	if(N % py > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by py (%d)\n", N, py); MPI_Finalize(); return 0; }
	if(N % pz > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by pz (%d)\n", N, pz); MPI_Finalize(); return 0; }

	nx = N / px;
	ny = N / py;
	nz = N / pz;

	if(nx % lx > 0) { if (isMainRank) printf("Error: nx (%d) should be divisible by lx (%d)\n", nx, lx); MPI_Finalize(); return 0; }
	if(ny % ly > 0) { if (isMainRank) printf("Error: ny (%d) should be divisible by ly (%d)\n", ny, ly); MPI_Finalize(); return 0; }
	if(nz % lz > 0) { if (isMainRank) printf("Error: nz (%d) should be divisible by lz (%d)\n", nz, lz); MPI_Finalize(); return 0; }

  double *U, *Un, *b;

  int fx = nx + 2 * gDepth;
  int fy = ny + 2 * gDepth;
  int fz = nz + 2 * gDepth;

	if (localId == 0)
	{
    U  = (double *)calloc(sizeof(double),fx*fy*fz);
    Un = (double *)calloc(sizeof(double),fx*fy*fz);
	}

	Mate_LocalBcast(&U,  sizeof(double *), 0);
	Mate_LocalBcast(&Un, sizeof(double *), 0);
	Mate_LocalBcast(&b,  sizeof(double *), 0);

 int* globalRankX = (int*) calloc(sizeof(int),processCount);
 int* globalRankY = (int*) calloc(sizeof(int),processCount);
 int* globalRankZ = (int*) calloc(sizeof(int),processCount);
 int*** globalProcessMapping = (int***) calloc(sizeof(int**),pz);
 for (int i = 0; i < pz; i++) globalProcessMapping[i] = (int**) calloc (sizeof(int*),py);
 for (int i = 0; i < pz; i++) for (int j = 0; j < py; j++) globalProcessMapping[i][j] = (int*) calloc (sizeof(int),px);

 int subMapCountX = px / mx;
 int subMapCountY = py / my;
 int subMapCountZ = pz / mz;
 int currentRank = 0;

 for (int sz = 0; sz < subMapCountZ; sz++)
 for (int sy = 0; sy < subMapCountY; sy++)
	for (int sx = 0; sx < subMapCountX; sx++)
	for (int z = sz * mz; z < (sz+1) * mz; z++)
	for (int y = sy * my; y < (sy+1) * my; y++)
	for (int x = sx * mx; x < (sx+1) * mx; x++)
	{ globalRankZ[currentRank] = z; globalRankX[currentRank] = x; globalRankY[currentRank] = y; globalProcessMapping[z][y][x] = currentRank; currentRank++; }

//  if (processId == 0) for (int i = 0; i < processCount; i++) printf("Rank %d - Z: %d, Y: %d, X: %d\n", i, globalRankZ[i], globalRankY[i], globalRankX[i]);

	int curLocalRank = 0;
	int*** localRankMapping = (int***) calloc (sizeof(int**) , lz);
	for (int i = 0; i < lz; i++) localRankMapping[i] = (int**) calloc (sizeof(int*) , ly);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) localRankMapping[i][j] = (int*) calloc (sizeof(int) , lx);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) for (int k = 0; k < lx; k++) localRankMapping[i][j][k] = curLocalRank++;

	for(int i = 0; i < lz; i++)
 for (int j = 0; j < ly; j++)
	for (int k = 0; k < lx; k++)
	 if (localRankMapping[i][j][k] == localId)
			{ localRank.z = i;  localRank.y = j; localRank.x = k;}

	start.x = (nx / lx) * localRank.x + gDepth;
	start.y = (ny / ly) * localRank.y + gDepth;
	start.z = (nz / lz) * localRank.z + gDepth;
	end.x = start.x + (nx / lx);
	end.y = start.y + (ny / ly);
	end.z = start.z + (nz / lz);

 globalProcess.z = globalRankZ[processId];
 globalProcess.y = globalRankY[processId];
 globalProcess.x = globalRankX[processId];

	West.type  = LOCAL; West.processId  = processId;
	East.type  = LOCAL; East.processId  = processId;
	North.type = LOCAL; North.processId = processId;
	South.type = LOCAL; South.processId = processId;
	Up.type    = LOCAL; Up.processId    = processId;
	Down.type  = LOCAL; Down.processId  = processId;

	Position* localRankArray = (Position*) calloc (sizeof(Position) , globalRankCount);
	Mate_Allgather(&localRank, 1, posType, localRankArray, 1, posType, MPI_COMM_WORLD);

	int* rankProcessArray = (int*) calloc (sizeof(int) , globalRankCount);
	Mate_Allgather(&processId, 1, MPI_INT, rankProcessArray, 1, MPI_INT, MPI_COMM_WORLD);

	int* LocalIdArray = (int*) calloc (sizeof(int) , globalRankCount);
	Mate_Allgather(&localId, 1, MPI_INT, LocalIdArray, 1, MPI_INT, MPI_COMM_WORLD);

	West.localRank.x  = localRank.x - 1; West.localRank.y  = localRank.y;     West.localRank.z  = localRank.z;
	East.localRank.x  = localRank.x + 1; East.localRank.y  = localRank.y;     East.localRank.z  = localRank.z;
	North.localRank.x = localRank.x;     North.localRank.y = localRank.y - 1; North.localRank.z = localRank.z;
	South.localRank.x = localRank.x;     South.localRank.y = localRank.y + 1; South.localRank.z = localRank.z;
	Up.localRank.x    = localRank.x;     Up.localRank.y    = localRank.y;     Up.localRank.z    = localRank.z - 1;
	Down.localRank.x  = localRank.x;     Down.localRank.y  = localRank.y;     Down.localRank.z  = localRank.z + 1;

	if (West.localRank.x  == -1) { West.type  = REMOTE;  West.localRank.x = lx-1; }
 if (East.localRank.x  == lx) { East.type  = REMOTE;  East.localRank.x = 0;    }
	if (North.localRank.y == -1) { North.type = REMOTE; North.localRank.y = ly-1; }
	if (South.localRank.y == ly) { South.type = REMOTE; South.localRank.y = 0;    }
	if (Up.localRank.z    == -1) { Up.type    = REMOTE;    Up.localRank.z = lz-1; }
	if (Down.localRank.z  == lz) { Down.type  = REMOTE;  Down.localRank.z = 0;    }

	if (globalProcess.x == 0    && localRank.x == 0)    West.type  = BOUNDARY;
	if (globalProcess.y == 0    && localRank.y == 0)    North.type = BOUNDARY;
	if (globalProcess.z == 0    && localRank.z == 0)    Up.type    = BOUNDARY;
	if (globalProcess.x == px-1 && localRank.x == lx-1) East.type  = BOUNDARY;
	if (globalProcess.y == py-1 && localRank.y == ly-1) South.type = BOUNDARY;
	if (globalProcess.z == pz-1 && localRank.z == lz-1) Down.type  = BOUNDARY;

	West.localId  = localRankMapping[localRank.z][localRank.y][West.localRank.x];
	East.localId  = localRankMapping[localRank.z][localRank.y][East.localRank.x];
	North.localId = localRankMapping[localRank.z][North.localRank.y][localRank.x];
	South.localId = localRankMapping[localRank.z][South.localRank.y][localRank.x];
	Up.localId    = localRankMapping[Up.localRank.z][localRank.y][localRank.x];
	Down.localId  = localRankMapping[Down.localRank.z][localRank.y][localRank.x];

	if (West.type  == REMOTE) West.processId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x-1];
	if (East.type  == REMOTE) East.processId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x+1];
	if (North.type == REMOTE) North.processId = globalProcessMapping[globalProcess.z][globalProcess.y-1][globalProcess.x];
	if (South.type == REMOTE) South.processId = globalProcessMapping[globalProcess.z][globalProcess.y+1][globalProcess.x];
	if (Up.type    == REMOTE) Up.processId    = globalProcessMapping[globalProcess.z-1][globalProcess.y][globalProcess.x];
	if (Down.type  == REMOTE) Down.processId  = globalProcessMapping[globalProcess.z+1][globalProcess.y][globalProcess.x];

 if (West.type  == LOCAL) Mate_AddLocalNeighbor(West.localId);
 if (East.type  == LOCAL) Mate_AddLocalNeighbor(East.localId);
 if (North.type == LOCAL) Mate_AddLocalNeighbor(North.localId);
 if (South.type == LOCAL) Mate_AddLocalNeighbor(South.localId);
 if (Up.type    == LOCAL) Mate_AddLocalNeighbor(Up.localId);
 if (Down.type  == LOCAL) Mate_AddLocalNeighbor(Down.localId);

	free(globalProcessMapping);
	free(localRankMapping);
 free(localRankArray);
	free(rankProcessArray);

 size.x = nx / lx;  size.y = ny / ly; size.z = nz / lz;
	int southTAG=1, northTAG=2, eastTAG=3, westTAG=4, downTAG=5, upTAG=6;

 MPI_Datatype faceXX_type;
 MPI_Datatype faceY_type;
 MPI_Datatype faceX_type;
 MPI_Datatype faceZ_type;

 MPI_Type_vector(size.y, 1, fx, MPI_DOUBLE, &faceX_type); MPI_Type_commit(&faceX_type);
 int* counts = (int*) calloc (sizeof(int) , size.z);
 MPI_Aint* displs = (MPI_Aint*) calloc (sizeof(MPI_Aint) , size.z);
 MPI_Datatype* types = (MPI_Datatype*) calloc (sizeof(MPI_Datatype) , size.z);
 for (int i = 0; i < size.z; i++) { counts[i] = 1;  displs[i] = fy*fx*sizeof(double)*i; types[i] = faceX_type; }
 MPI_Type_struct(size.z / sizeDivider, counts, displs, types, &faceXX_type); MPI_Type_commit(&faceXX_type);
 MPI_Type_vector(size.z / sizeDivider, size.x, fx*fy, MPI_DOUBLE, &faceY_type); MPI_Type_commit(&faceY_type);
 MPI_Type_vector(size.y / sizeDivider, size.x, fx, MPI_DOUBLE, &faceZ_type); MPI_Type_commit(&faceZ_type);

	int count_down  = Down.type  == REMOTE ? count : 0;
	int count_up    = Up.type    == REMOTE ? count : 0;
	int count_east  = East.type  == REMOTE ? count : 0;
	int count_west  = West.type  == REMOTE ? count : 0;
	int count_north = North.type == REMOTE ? count : 0;
	int count_south = South.type == REMOTE ? count : 0;

 int nodeId = -1;
	char* nodeIdString = getenv("SLURM_NODEID");
	if (nodeIdString != NULL) nodeId = atoi(nodeIdString);

 int* nodeIdArray = (int*) malloc (sizeof(int) * globalRankCount);
 Mate_Allgather(&nodeId, 1, MPI_INT, nodeIdArray, 1, MPI_INT, MPI_COMM_WORLD);

 for (int k = start.z-gDepth; k < end.z+gDepth; k++)
	for (int j = start.y-gDepth; j < end.y+gDepth; j++)
	for (int i = start.x-gDepth; i < end.x+gDepth; i++)
		 U[k*fy*fx + j*fx + i] = 1;

	if (West.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + d] = 0;
	if (North.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + d*fx + i] = 0;
	if (Up.type    == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[d*fx*fy + j*fx + i] = 0;
	if (East.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + (nx+gDepth+d)] = 0;
	if (South.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + (ny+gDepth+d)*fx + i] = 0;
	if (Down.type  == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[(nz+gDepth+d)*fx*fy + j*fx + i] = 0;

 int request_count = 0;
 int pos = 0;

 int* countFaceX = (int*) calloc (sizeof(int), nMessages);
	int* countFaceY = (int*) calloc (sizeof(int), nMessages);
	int* countFaceZ = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceX = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceY = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceZ = (int*) calloc (sizeof(int), nMessages);

	int bufferSizeX = sizeMultiplier*size.y*size.z;
	int bufferSizeY = sizeMultiplier*size.x*size.z;
	int bufferSizeZ = sizeMultiplier*size.x*size.y;

	bufferSizeX = bufferSizeX / sizeDivider;
	bufferSizeY = bufferSizeY / sizeDivider;
	bufferSizeZ = bufferSizeZ / sizeDivider;

	int msgSizeX = bufferSizeX / nMessages;
	int msgSizeY = bufferSizeY / nMessages;
	int msgSizeZ = bufferSizeZ / nMessages;

	countFaceX[0] = msgSizeX;
	countFaceY[0] = msgSizeY;
	countFaceZ[0] = msgSizeZ;

	offsetFaceX[0] = 0;
	offsetFaceY[0] = 0;
	offsetFaceZ[0] = 0;

	for (int i = 1; i < nMessages; i++)
	{
		countFaceX[i] = msgSizeX;
		countFaceY[i] = msgSizeY;
		countFaceZ[i] = msgSizeZ;

		offsetFaceX[i] = offsetFaceX[i-1] + msgSizeX;
		offsetFaceY[i] = offsetFaceY[i-1] + msgSizeY;
		offsetFaceZ[i] = offsetFaceZ[i-1] + msgSizeZ;
	}

	countFaceX[nMessages-1] = bufferSizeX - offsetFaceX[nMessages-1];
	countFaceY[nMessages-1] = bufferSizeY - offsetFaceY[nMessages-1];
	countFaceZ[nMessages-1] = bufferSizeZ - offsetFaceZ[nMessages-1];

	double** upSendBuffer    = (double**) calloc (sizeof(double*),gDepth);
	double** downSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** eastSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** westSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** northSendBuffer = (double**) calloc (sizeof(double*),gDepth);
	double** southSendBuffer = (double**) calloc (sizeof(double*),gDepth);
	double** upRecvBuffer    = (double**) calloc (sizeof(double*),gDepth);
	double** downRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** eastRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** westRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
	double** northRecvBuffer = (double**) calloc (sizeof(double*),gDepth);
	double** southRecvBuffer = (double**) calloc (sizeof(double*),gDepth);

	for (int i = 0; i < gDepth; i++)
	{
		upSendBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
		downSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
		eastSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		westSendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		northSendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		southSendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		upRecvBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
		downRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
		eastRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		westRecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
		northRecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		southRecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
	}

	MPI_Request request[24*gDepth*nMessages];

 double computeTime = 0;
 double packTime = 0;
 double unpackTime = 0;
 double sendTime = 0;
 double recvTime = 0;
 double waitTime = 0;
 double scheduleTime = 0;
	double t_init;

 MPI_Barrier(MPI_COMM_WORLD);

 double execTime = -Mate_Wtime();

	#pragma mate graph
	for (int iter=0; iter<nIters; iter++)
 {
		#pragma mate region(compute) depends(pack*, unpack*, compute*@)
		{
		 t_init = Mate_Wtime();

			request_count =0;
			for (int k0 = start.z; k0 < end.z; k0+=BLOCKZ) {
				int k1= k0+BLOCKZ<end.z?k0+BLOCKZ:end.z;
				for (int j0 = start.y; j0 < end.y; j0+=BLOCKY) {
					int j1= j0+BLOCKY<end.y?j0+BLOCKY:end.y;
					for (int k = k0; k < k1; k++) {
						 for (int j = j0; j < j1; j++){
								#pragma vector aligned
								#pragma ivdep
								for (int i = start.x; i < end.x; i++)
								{
									double sum = 0;
									sum += U[fx*fy*k     + fx*j         + i] * stencil_coeff[0]; // Central
									for (int d = 1; d <= gDepth; d++)
									{
										double partial_sum = 0;
										partial_sum += U[fx*fy*(k-d) + fx*j         + i]; // Up
										partial_sum += U[fx*fy*(k+d) + fx*j         + i]; // Down
										partial_sum += U[fx*fy*k     + fx*j     - d + i]; // East
										partial_sum += U[fx*fy*k     + fx*j     + d + i]; // West
										partial_sum += U[fx*fy*k     + fx*(j+d)     + i]; // North
										partial_sum += U[fx*fy*k     + fx*(j-d)     + i]; // South
										sum += partial_sum * stencil_coeff[d];
									}
									Un[fx*fy*k     + fx*j + i] = sum; // Update
								}
						 }
					}
				}
			}
			double *temp = NULL;
			temp = U;
			U = Un;
			Un = temp;

			computeTime += Mate_Wtime() -t_init;
		}

		#pragma mate region (request)  depends (unpack*)
		{
		 t_init = Mate_Wtime();

		 if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(downRecvBuffer[d]  + offsetFaceZ[i],  count_down*countFaceZ[i], MPI_DOUBLE, Down.processId,  Down.localId,  upTAG,    MPI_COMM_WORLD, &request[request_count++]);
		 if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(upRecvBuffer[d]    + offsetFaceZ[i],    count_up*countFaceZ[i], MPI_DOUBLE, Up.processId,    Up.localId,    downTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(eastRecvBuffer[d]  + offsetFaceX[i],  count_east*countFaceX[i], MPI_DOUBLE, East.processId,  East.localId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(westRecvBuffer[d]  + offsetFaceX[i],  count_west*countFaceX[i], MPI_DOUBLE, West.processId,  West.localId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(northRecvBuffer[d] + offsetFaceY[i], count_north*countFaceY[i], MPI_DOUBLE, North.processId, North.localId, southTAG, MPI_COMM_WORLD, &request[request_count++]);
		 if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Recv(southRecvBuffer[d] + offsetFaceY[i], count_south*countFaceY[i], MPI_DOUBLE, South.processId, South.localId, northTAG, MPI_COMM_WORLD, &request[request_count++]);

		 recvTime += Mate_Wtime() - t_init;
		}

		#pragma mate region (pack)  depends (compute, send*)
		{
			t_init = Mate_Wtime();

		 if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*(end.z-gDepth+d) + fx*start.y            + start.x              ],   packCount*count_down,  faceZ_type,  downSendBuffer[d],    sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*(start.z+d)      + fx*start.y            + start.x              ],   packCount*count_up,    faceZ_type,  upSendBuffer[d],  sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*start.z          + fx*start.y            + (end.x-gDepth+d)     ],   packCount*count_east,  faceXX_type, eastSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*start.z          + fx*start.y            + (start.x+d)          ],   packCount*count_west,  faceXX_type, westSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*start.z          + fx*(start.y+d)        + start.x              ],   packCount*count_north, faceY_type,  northSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Pack(&U[fx*fy*start.z          + fx*(end.y-gDepth+d)   + start.x              ],   packCount*count_south, faceY_type,  southSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}

		 packTime += Mate_Wtime() - t_init;
		}

		#pragma mate region (send) depends (pack)
		{
			t_init = Mate_Wtime();

		 if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(downSendBuffer[d]  + offsetFaceZ[i],   count_down*countFaceZ[i], MPI_DOUBLE,  Down.processId,  Down.localId, downTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(upSendBuffer[d]    + offsetFaceZ[i],     count_up*countFaceZ[i], MPI_DOUBLE,  Up.processId,    Up.localId,    upTAG,    MPI_COMM_WORLD, &request[request_count++]);
		 if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(eastSendBuffer[d]  + offsetFaceX[i],   count_east*countFaceX[i], MPI_DOUBLE,  East.processId,  East.localId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(westSendBuffer[d]  + offsetFaceX[i],   count_west*countFaceX[i], MPI_DOUBLE,  West.processId,  West.localId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
		 if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(northSendBuffer[d] + offsetFaceY[i],  count_north*countFaceY[i], MPI_DOUBLE,  North.processId, North.localId, northTAG, MPI_COMM_WORLD, &request[request_count++]);
		 if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) Mate_Send(southSendBuffer[d] + offsetFaceY[i],  count_south*countFaceY[i], MPI_DOUBLE,  South.processId, South.localId, southTAG, MPI_COMM_WORLD, &request[request_count++]);

		 sendTime += Mate_Wtime() - t_init;
		}

		#pragma mate region (unpack) depends (compute, request)
		{
		 t_init = Mate_Wtime();

		 if(Down.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(downRecvBuffer[d],  packCount*count_down*sizeof(double)*size.y*size.x,  &pos, &U[fx*fy*(end.z+d) + fx*start.y     + start.x   ],   packCount*count_down,  faceZ_type, MPI_COMM_WORLD);  pos = 0;}
		 if(Up.type    == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(upRecvBuffer[d],    packCount*count_up*sizeof(double)*size.y*size.x,    &pos, &U[fx*fy*d         + fx*start.y     + start.x   ],   packCount*count_up,    faceZ_type,  MPI_COMM_WORLD); pos = 0;}
		 if(East.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(eastRecvBuffer[d],  packCount*count_east*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y     + end.x + d ],   packCount*count_east,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		 if(West.type  == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(westRecvBuffer[d],  packCount*count_west*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y     + d         ],   packCount*count_west,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		 if(North.type == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(northRecvBuffer[d], packCount*count_north*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*d           + start.x   ],   packCount*count_north, faceY_type, MPI_COMM_WORLD);  pos = 0;}
		 if(South.type == REMOTE) for (int d = 0; d < gDepth; d++) { Mate_Unpack(southRecvBuffer[d], packCount*count_south*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*(end.y+d)   + start.x   ],   packCount*count_south, faceY_type, MPI_COMM_WORLD);  pos = 0;}

		 unpackTime += Mate_Wtime() - t_init;
		}
 }

	MPI_Barrier(MPI_COMM_WORLD);
	execTime += Mate_Wtime();

 double res = 0;
 double err = 0;
 for (int k=start.z; k<end.z; k++)
	for (int j=start.y; j<end.y; j++)
	for (int i=start.x; i<end.x; i++)
  { double r = U[k*fy*fx + j*fx + i];  err += r * r; }
 MPI_Reduce (&err, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

 double meanComputeTime = 0, sumComputeTime = 0;
 double meanPackTime = 0, sumPackTime = 0;
 double meanUnpackTime = 0, sumUnpackTime = 0;
 double meanSendTime = 0, sumSendTime = 0;
 double meanRecvTime = 0, sumRecvTime = 0;
 double meanWaitTime = 0, sumWaitTime = 0;
 double meanScheduleTime = 0, sumScheduleTime = 0;

 MPI_Reduce(&computeTime, &sumComputeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&packTime, &sumPackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&unpackTime, &sumUnpackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&sendTime, &sumSendTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&recvTime, &sumRecvTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&waitTime, &sumWaitTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 MPI_Reduce(&scheduleTime, &sumScheduleTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

 int threadCount = 1;
 Mate_local_worker_count(&threadCount);

 meanComputeTime  = sumComputeTime / (threadCount * processCount);
 meanPackTime     = sumPackTime / (threadCount * processCount);
 meanUnpackTime   = sumUnpackTime / (threadCount * processCount);
 meanSendTime     = sumSendTime / (threadCount * processCount);
 meanRecvTime     = sumRecvTime / (threadCount * processCount);
 meanScheduleTime = sumScheduleTime / (threadCount * processCount);
 meanWaitTime     = sumWaitTime / (threadCount * processCount);

	if(isMainRank)
	{
	  double gflops = nIters*(double)N*(double)N*(double)N*(2 + gDepth * 8)/(1.0e9);
	  printf("Compute:     %.4f\n", meanComputeTime);
	  printf("Mate_Recv:   %.4f\n", meanRecvTime);
	  printf("Mate_Send:   %.4f\n", meanSendTime);
	  printf("Mate_Pack:   %.4f\n", meanPackTime);
	  printf("Mate_Unpack: %.4f\n", meanUnpackTime);
	  printf("Wait Time:   %.4f\n", meanScheduleTime);
	  printf("%.4fs, %.3f GFlop/s (L2 Norm: %.10g)\n", execTime, gflops/execTime, res);
	}

 Mate_Finalize();
 return 0;
}
